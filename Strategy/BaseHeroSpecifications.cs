﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public abstract class BaseHeroSpecifications
    {
        public ITypeDamaging damageType;
        public IRangeAttackable rangeAttack;

        public void Run()
        {
            Console.WriteLine("I can move");
        }

        public void SetRangeAttack(IRangeAttackable newRangeAttackable)
        {
            rangeAttack = newRangeAttackable;
        }

        public void SetTypeAttack(ITypeDamaging newTypeDamaging)
        {
            damageType = newTypeDamaging;
        }

        public abstract void Display();
    }

}
