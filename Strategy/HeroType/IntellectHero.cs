﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.RangeType;
using Strategy.TypeDamage;

namespace Strategy
{
    public class IntellectHero : BaseHeroSpecifications
    {
        public IntellectHero()
        {
            rangeAttack = new HighRange();
            damageType = new MagicDamage();
        }
        public override void Display()
        {
            Console.WriteLine("I'm an intellectHero");
        }
    }
}
