﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.TypeDamage;

namespace Strategy.HeroType
{
    public class StrengthHero : BaseHeroSpecifications
    {
        public StrengthHero()
        {
            damageType = new PhysicalDamage();
            rangeAttack = new LowRange();
        }
        public override void Display()
        {
            Console.WriteLine("I'm an strength hero");
        }
    }
}
