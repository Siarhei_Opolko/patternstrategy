﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.HeroType;
using Strategy.TypeDamage;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            var heroes = new List<BaseHeroSpecifications>();
            heroes.Add(new IntellectHero());
            heroes.Add(new StrengthHero());

            foreach (var hero in heroes)
            {
                hero.Display();
                hero.damageType.ShowTypeDamage();
                hero.rangeAttack.ShowRangeAttack();
                hero.Run();
                Console.WriteLine("");
            }
           
            IntellectHero iHero = new IntellectHero();
            iHero.Display();
            iHero.damageType.ShowTypeDamage();
            iHero.SetTypeAttack(new PhysicalDamage());
            iHero.damageType.ShowTypeDamage();
            Console.ReadKey();
        }
    }
}
