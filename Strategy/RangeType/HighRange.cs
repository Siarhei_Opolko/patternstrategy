﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.RangeType
{
    public class HighRange : IRangeAttackable
    {
        public void ShowRangeAttack()
        {
            Console.WriteLine("I have high range attack now");
        }
    }
}
