﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.TypeDamage
{
    public class PhysicalDamage : ITypeDamaging
    {
       public void ShowTypeDamage()
       {
           Console.WriteLine("I have physical type damage now");
       }
    }
}
